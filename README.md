# This repo has moved to GitHub

## Issue Tracker

The issue tracker is now at [https://github.com/IBM/ibmi-oss-issues](https://github.com/IBM/ibmi-oss-issues)

## Documentation

The docs are now at [https://ibmi-oss-docs.readthedocs.io/en/latest](https://ibmi-oss-docs.readthedocs.io/en/latest).
The repo is now at [https://github.com/IBM/ibmi-oss-docs](https://github.com/IBM/ibmi-oss-docs)

